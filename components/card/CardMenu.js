import { useState } from 'react';
import styles from './CardMenu.module.css';

export default function CardMenu(props) {
  const [counter, setCounter] = useState(0);
  const [test, setTest] = useState({});

  const handleMinus = () => {
    if (counter > 0) {
      setCounter(counter - 1);
    }
  };

  const handlePlus = () => {
    setCounter(counter + 1);
  };

  return (
    <div>
      <div className={styles.menus}>Order:</div>
      {props.menus.map((i, idx) => (
        <div className={styles.submenus} key={idx}>
          <div className={styles.food}>{i.food}</div>
          <div>{i.price}</div>
          <div className={styles.counter}>
            <button className={styles.but1} onClick={handleMinus}>
              -
            </button>
            <button className={styles.but2} onClick={handlePlus}>
              +
            </button>
          </div>
        </div>
      ))}
      <div className={styles.bottom}>
        <button className={styles.cancel} onClick={() => setCounter(0)}>
          Cancel
        </button>
        <div className={styles.total}>Total Food: {counter}</div>
      </div>
    </div>
  );
}

import Image from 'next/image';
import { useRouter } from 'next/router';
import styles from './Card.module.css';

export default function Card(props) {
  const classes = [styles.card, styles[props.type]].filter(Boolean).join(' ');
  const router = useRouter();

  const DetailRestoHandler = () => {
    router.push('/favorite/' + props.id);
  };

  return (
    <div className={classes} onClick={DetailRestoHandler}>
      <div className={styles.address}>{props.address}</div>
      <Image
        alt="img"
        className={styles.image}
        src={props.image}
        width={250}
        height={230}
      />
      <div className={styles.container}>
        <p className={styles.rating}>Rating: {props.rating}</p>
        <h4>
          <b className={styles.name}>{props.name}</b>
        </h4>
        <p className={styles.description}>{props.description}</p>

        {props.type === 'large' && (
          <>
            <hr />
            <div className={styles.menus}>Food Menu:</div>
            {props.menus.map((i, idx) => (
              <div className={styles.submenus} key={idx}>
                <div>{i.food}</div>
                <div>{i.price}</div>
              </div>
            ))}
          </>
        )}
      </div>
    </div>
  );
}

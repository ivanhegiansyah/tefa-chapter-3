import styles from '../../styles/Account.module.css';
import { signIn, signOut, getSession } from 'next-auth/react';

export default function Account({ session }) {
  if (session) {
    return (
      <div className={styles.root}>
        <p>
          Signed in as <span className={styles.name}>{session.user.name}</span>
        </p>
        <button onClick={signOut}>Sign out</button>
      </div>
    );
  }
  return (
    <div className={styles.root}>
      <p>You have not sign in</p>
      <button onClick={signIn}>Sign in</button>
    </div>
  );
}

export const getServerSideProps = async (context) => {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
};

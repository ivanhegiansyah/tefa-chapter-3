import { useState } from 'react';
import { useForm } from 'react-hook-form';
import styles from '../../styles/About.module.css';

export default function About() {
  const [val, setVal] = useState();
  const [loading, setLoading] = useState(true);

  const { register, handleSubmit } = useForm();
  const onSubmit = (data) => {
    setVal(data);
    setLoading(false);
    if (!loading) {
      console.log(val);
    }
  };

  return (
    <div className={styles.root}>
      <div className={styles.title}>About Me</div>
      <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        <input
          type="text"
          placeholder="firstName"
          {...register('firstName', { required: true })}
        />
        <select {...register('category', { required: true })}>
          <option value="A">A</option>
          <option value=" B">B</option>
          <option value=" C">C</option>
        </select>
        <textarea
          placeholder="aboutYou"
          {...register('aboutYou', { required: true })}
        />
        {!loading && <div>{JSON.stringify(val)}</div>}
        <input type="submit" />
      </form>
    </div>
  );
}

import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { restaurants } from '../../restaurants';
import CardList from '../../components/card/CardList';
import styles from '../../styles/Favorite.module.css';
import CardMenu from '../../components/card/CardMenu';

export default function DetailRestaurant() {
  const [resto, setResto] = useState();
  const [loading, setLoading] = useState(true);
  const router = useRouter();
  const id = router.query.id;

  useEffect(() => {
    const filteredRestaurant = restaurants.filter(
      (restaurant) => restaurant.id === parseInt(id)
    );
    setResto(filteredRestaurant);
    setLoading(false);
  }, [id, loading]);

  return (
    <div className={styles.root}>
      <div className={styles.title}>Restaurant Detail</div>
      {!loading && <CardList data={resto} type={'large'} />}
      {!loading && <CardMenu menus={resto[0].menus} />}
    </div>
  );
}

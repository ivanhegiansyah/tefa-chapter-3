import { restaurants } from '../../restaurants';
import styles from '../../styles/Favorite.module.css';
import CardList from '../../components/card/CardList';

export default function Favorite() {
  return (
    <div className={styles.root}>
      <div className={styles.title}>Restaurant Menus</div>
      <CardList data={restaurants} type={'large'} />
    </div>
  );
}

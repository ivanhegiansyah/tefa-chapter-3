import Image from 'next/image';
import CardList from '../components/card/CardList';
import styles from '../styles/Home.module.css';
import { restaurants } from '../restaurants';

export default function Home() {
  return (
    <>
      <div className={styles['img-full-width']}>
        <Image
          alt="food-banner"
          src="https://images.unsplash.com/photo-1505935428862-770b6f24f629?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1734&q=80"
          width={10000}
          height={4500}
        />
      </div>
      <div>
        <div className={styles['sub-title']}>Explore Restaurant</div>
        <CardList data={restaurants} type={'small'} />
      </div>
    </>
  );
}

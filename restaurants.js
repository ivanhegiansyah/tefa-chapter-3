export const restaurants = [
  {
    id: 1,
    name: 'First Resto',
    image:
      'https://images.unsplash.com/photo-1552566626-52f8b828add9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
    address: 'Kota. Bandung',
    description: 'Sunt ut cillum ad in esse Lorem consequat incididunt.',
    rating: 4.5,
    menus: [
      {
        food: 'Bread',
        price: 'Rp. 25.000',
      },
      {
        food: 'Pizza',
        price: 'Rp. 50.000',
      },
      {
        food: 'Sushi',
        price: 'Rp. 30.000',
      },
    ],
  },
  {
    id: 2,
    name: 'Second Resto',
    image:
      'https://images.unsplash.com/photo-1552566626-52f8b828add9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
    address: 'Kota. Bandung',
    description:
      'Amet non cillum duis aliqua occaecat fugiat incididunt quis do eu amet consectetur mollit.',
    rating: 4.5,
    menus: [
      {
        food: 'Bread',
        price: 'Rp. 25.000',
      },
      {
        food: 'Pizza',
        price: 'Rp. 50.000',
      },
      {
        food: 'Sushi',
        price: 'Rp. 30.000',
      },
    ],
  },
  {
    id: 3,
    name: 'Third Resto',
    image:
      'https://images.unsplash.com/photo-1552566626-52f8b828add9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
    address: 'Kota. Bandung',
    description:
      'Est ullamco tempor ipsum tempor labore minim culpa culpa velit.',
    rating: 4.5,
    menus: [
      {
        food: 'Bread',
        price: 'Rp. 25.000',
      },
      {
        food: 'Pizza',
        price: 'Rp. 50.000',
      },
      {
        food: 'Sushi',
        price: 'Rp. 30.000',
      },
    ],
  },
];
